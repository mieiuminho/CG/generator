#include "Patch.h"
#include "../utils/Calculator.h"

vector<int> Patch ::parseIndexes(const char *line) {

  vector<int> r;
  string token;
  istringstream tokenStream(line);

  while (getline(tokenStream, token, ',')) {
    r.push_back(stoi(token));
  }

  return r;
}

float *Patch ::parsePoint(const char *line) {

  float *r = (float *)malloc(sizeof(float) * 3);
  string token;
  istringstream tokenStream(line);

  int i = 0;

  while (getline(tokenStream, token, ',')) {
    r[i++] = stof(token);
  }

  return r;
}

vector<Point_3D> Patch ::get_patch(int index) {

  int dimension = this->patches_indexes.at(index).size();
  vector<Point_3D> r;

  for (int i = 0; i < dimension; i++)
    r.push_back(this->control_points.at(this->patches_indexes.at(index).at(i)));

  return r;
}

Point_3D Patch ::point_at(int patch, int index) {
  return this->control_points.at(this->patches_indexes.at(patch).at(index));
}

vector<vector<Point_3D>> Patch ::construct_patch(int index, int tesselation) {

  vector<Point_3D> control_points = this->get_patch(index);

  vector<vector<Point_3D>> curve_points;

  for (int i = 0; i <= tesselation; i++) {

    vector<Point_3D> points;

    float u_slice = (float)i / (float)tesselation;

    for (int j = 0; j <= tesselation; j++) {

      float v_slice = (float)j / (float)tesselation;

      Point_3D p = Point_3D();
      p.setX(0);
      p.setY(0);
      p.setZ(0);

      Point_3D P[4][4];
      for (int m = 0; m <= 3; m++) {
        for (int n = 0; n <= 3; n++) {
          P[m][n] = control_points.at(4 * m + n);
        }
      }

      float M[4][4] = {
          {-1, 3, -3, 1}, {3, -6, 3, 0}, {-3, 3, 0, 0}, {1, 0, 0, 0}};
      float V[4] = {v_slice * v_slice * v_slice, v_slice * v_slice, v_slice, 1};
      float dU[4] = {3 * u_slice * u_slice, 2 * u_slice, 1, 0};
      float u_m[4];
      Calculator ::vector_by_matrix_mul(dU, M, u_m);
      Point_3D res[4];
      Calculator ::vector_by_point_matrix(u_m, P, res);
      Point_3D res2[4];
      Calculator ::point_vector_by_matrix(res, M, res2);
      Point_3D partial_u = Calculator ::point_vector_by_vector(res2, V);

      points.push_back(partial_u);
    }

    curve_points.push_back(points);
  }

  return curve_points;
}

vector<vector<Point_3D>> Patch ::construct_patch_normals(int index,
                                                         int tesselation) {

  vector<Point_3D> control_points = this->get_patch(index);

  vector<vector<Point_3D>> normals;

  for (int i = 0; i <= tesselation; i++) {

    vector<Point_3D> points;

    float u_slice = (float)i / (float)tesselation;

    for (int j = 0; j <= tesselation; j++) {

      float v_slice = (float)j / (float)tesselation;

      Point_3D P[4][4];
      for (int m = 0; m < 4; m++) {
        for (int n = 0; n < 4; n++) {
          P[m][n] = control_points.at(4 * m + n);
        }
      }

      // calcular a normal
      float M[4][4] = {
          {-1, 3, -3, 1}, {3, -6, 3, 0}, {-3, 3, 0, 0}, {1, 0, 0, 0}};
      float U[4] = {u_slice * u_slice * u_slice, u_slice * u_slice, u_slice, 1};
      float V[4] = {v_slice * v_slice * v_slice, v_slice * v_slice, v_slice, 1};
      float dU[4] = {3 * u_slice * u_slice, 2 * u_slice, 1, 0};
      float u_m[4];
      Calculator ::vector_by_matrix_mul(dU, M, u_m);
      Point_3D res[4];
      Calculator ::vector_by_point_matrix(u_m, P, res);
      Point_3D res2[4];
      Calculator ::point_vector_by_matrix(res, M, res2);
      Point_3D partial_u = Calculator ::point_vector_by_vector(res2, V);

      float v_m[4];
      Calculator ::vector_by_matrix_mul(U, M, v_m);
      Point_3D resV[4];
      Calculator ::vector_by_point_matrix(v_m, P, resV);
      Point_3D resV2[4];
      Calculator ::point_vector_by_matrix(resV, M, resV2);
      Point_3D partial_v = Calculator ::point_vector_by_vector(resV2, V);

      Point_3D normal = partial_u.crossProduct(partial_v);

      points.push_back(normal);
    }

    normals.push_back(points);
  }

  return normals;
}

Model Patch ::constructPatchedModel(int tesselation) {

  vector<SimplePlan> plans;

  int no_points = 0;
  int no_indexes = 0;

  vector<Point_3D> normals;

  vector<Point_2D> texturePoints;

  for (int i = 0; i < this->number_patches; i++) {

    vector<vector<Point_3D>> patched_model_points =
        this->construct_patch(i, tesselation);

    vector<vector<Point_3D>> patched_model_normals =
        this->construct_patch_normals(i, tesselation);

    float delta = 1.0f / (float)tesselation;

    float texture_x_step = 1.0f / (float)(tesselation);

    for (int j = 0; j < tesselation; j++) {

      float texture_y_step = 1.0f / (float)(tesselation);

      float u = delta * j;
      float u_next = delta * (j + 1);

      for (int k = 0; k < tesselation; k++) {

        float v = delta * k;
        float v_next = delta * (k + 1);

        Point_3D p1 = this->bezier_point(i, u, v_next);
        Point_3D p2 = this->bezier_point(i, u, v);
        Point_3D p3 = this->bezier_point(i, u_next, v_next);
        Point_3D p4 = this->bezier_point(i, u_next, v);

        Point_3D normal_p1 = this->bezier_normal(i, u, v_next);
        Point_3D normal_p2 = this->bezier_normal(i, u, v);
        Point_3D normal_p3 = this->bezier_normal(i, u_next, v_next);
        Point_3D normal_p4 = this->bezier_normal(i, u_next, v);

        no_points += 4;
        no_indexes += 6;

        normals.push_back(normal_p1);
        normals.push_back(normal_p2);
        normals.push_back(normal_p3);
        normals.push_back(normal_p4);

        texturePoints.emplace_back((k + 1) * texture_x_step,
                                   j * texture_y_step);
        texturePoints.emplace_back(k * texture_x_step, j * texture_y_step);
        texturePoints.emplace_back((k + 1) * texture_x_step,
                                   (j + 1) * texture_y_step);
        texturePoints.emplace_back((k)*texture_x_step,
                                   (j + 1) * texture_y_step);

        plans.emplace_back(p1, p2, p3, p4, -1);
      }
    }
  }

  return {plans, no_points, no_indexes, normals, texturePoints};
}

Point_3D Patch ::bezier_point(int patch, float du, float dv) {
  Point_3D P[4][4];
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      P[i][j] = this->point_at(patch, i * 4 + j);
    }
  }
  float M[4][4] = {{-1, 3, -3, 1}, {3, -6, 3, 0}, {-3, 3, 0, 0}, {1, 0, 0, 0}};
  float U[4] = {du * du * du, du * du, du, 1};
  float V[4] = {dv * dv * dv, dv * dv, dv, 1};
  float u_m[4];
  Calculator ::vector_by_matrix_mul(U, M, u_m);
  Point_3D res[4];
  Calculator ::vector_by_point_matrix(u_m, P, res);
  Point_3D res2[4];
  Calculator ::point_vector_by_matrix(res, M, res2);
  Point_3D final = Calculator ::point_vector_by_vector(res2, V);
  return final;
}

Point_3D Patch ::bezier_normal(int patch, float du, float dv) {
  Point_3D P[4][4];
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      P[i][j] = this->point_at(patch, i * 4 + j);
    }
  }
  float M[4][4] = {{-1, 3, -3, 1}, {3, -6, 3, 0}, {-3, 3, 0, 0}, {1, 0, 0, 0}};
  float U[4] = {du * du * du, du * du, du, 1};
  float V[4] = {dv * dv * dv, dv * dv, dv, 1};
  float dU[4] = {3 * du * du, 2 * du, 1, 0};
  float dV[4] = {3 * dv * dv, 2 * dv, 1, 0};
  float u_m[4];
  Calculator ::vector_by_matrix_mul(dU, M, u_m);
  Point_3D res[4];
  Calculator ::vector_by_point_matrix(u_m, P, res);
  Point_3D res2[4];
  Calculator ::point_vector_by_matrix(res, M, res2);
  Point_3D partial_u = Calculator ::point_vector_by_vector(res2, V);

  float v_m[4];
  Calculator ::vector_by_matrix_mul(U, M, v_m);
  Point_3D res_2[4];
  Calculator ::vector_by_point_matrix(v_m, P, res_2);
  Point_3D res_2_2[4];
  Calculator ::point_vector_by_matrix(res_2, M, res_2_2);
  Point_3D partial_v = Calculator ::point_vector_by_vector(res_2_2, dV);

  Point_3D normal = partial_v.crossProduct(partial_u);

  normal.normalize();

  return normal;
}
