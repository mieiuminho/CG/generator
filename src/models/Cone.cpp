#include "Cone.h"

float Cone ::getBottomRadius() { return this->bottom_radius; }

float Cone ::getHeight() { return this->height; }

int Cone ::getStacks() { return this->stacks; }

int Cone ::getSlices() { return this->slices; }

Point_2D getTextureCoordinates(int curr_stack, int curr_slice, bool base) {

  if (base == true) {

  } else {
  }
}

Model Cone ::generateModel() {

  float slice_step = 2 * M_PI / ((float)this->slices);
  float stack_basic_height = height / ((float)this->stacks);
  float slice_angle, slice_angle_2;
  float stack_height, stack_height_2;
  float radius, radius_2;
  float x, y, z, x2, y2, z2, x3, y3, z3, x4, y4, z4;
  float beta = atanf(this->height / this->bottom_radius);
  float alpha_shift = 2 * M_PI / this->slices;

  int no_points = 0;
  int no_indexes = 0;

  Point_3D p1, p2, p3, p4;

  vector<SimplePlan> r;

  vector<Point_3D> normals;

  vector<Point_2D> texturePoints;

  float texture_x_step = 1.0f / (float)this->slices;
  float texture_y_step = 1.0f / (float)this->stacks;

  for (int i = 1; i <= this->stacks; i++) {

    stack_height = stack_basic_height * (i - 1);
    stack_height_2 = stack_basic_height * i;

    // Semelhança de triângulos
    radius = ((this->height - (stack_basic_height * (i - 1))) *
              this->bottom_radius) /
             this->height;
    radius_2 =
        ((this->height - (stack_basic_height * i)) * this->bottom_radius) /
        this->height;

    for (int j = 1; j <= this->slices; j++) {

      slice_angle = (float)(j - 1) * slice_step;
      slice_angle_2 = (float)j * slice_step;

      x = radius_2 * sinf(slice_angle);
      y = stack_height_2;
      z = radius_2 * cosf(slice_angle);

      x2 = radius_2 * sinf(slice_angle_2);
      y2 = stack_height_2;
      z2 = radius_2 * cosf(slice_angle_2);

      x3 = radius * sinf(slice_angle);
      y3 = stack_height;
      z3 = radius * cosf(slice_angle);

      x4 = radius * sinf(slice_angle_2);
      y4 = stack_height;
      z4 = radius * cosf(slice_angle_2);

      // Desenhar a base -> só necessitamos de uma iteração completa deste ciclo
      // para desenhar base (daí o if) Do cíclo mais a cima (o que é coordenado
      // pelo `i` )

      if (i == 1) {

        p1 = Point_3D(0, 0, 0);
        p3 = Point_3D(x3, 0, z3);
        p4 = Point_3D(x4, 0, z4);

        no_points += 3;
        no_indexes += 3;

        normals.emplace_back(0, -1, 0);
        normals.emplace_back(0, -1, 0);
        normals.emplace_back(0, -1, 0);

        texturePoints.emplace_back(0.5, 0.5);

        texturePoints.emplace_back(0.5 * cosf(slice_angle) + 0.5,
                                   0.5 * sinf(slice_angle) + 0.5);
        texturePoints.emplace_back(0.5 * cosf(slice_angle_2) + 0.5,
                                   0.5 * sinf(slice_angle_2) + 0.5);
        SimplePlan p = SimplePlan(p1, p3, p4, -1);

        r.push_back(p);
      }

      p1 = Point_3D(x, y, z);
      p2 = Point_3D(x2, y2, z2);
      p3 = Point_3D(x3, y3, z3);
      p4 = Point_3D(x4, y4, z4);

      float alpha = alpha_shift * (j - 1);
      float alpha_next = alpha_shift * (j);
      Point_3D sideNorm = {cosf(beta) * sinf(alpha), sinf(beta),
                           cosf(beta) * cos(alpha)};
      Point_3D nextSideNorm = {cosf(beta) * sinf(alpha_next), sinf(beta),
                               cosf(beta) * cos(alpha_next)};

      normals.push_back(sideNorm);
      normals.push_back(sideNorm);
      normals.push_back(nextSideNorm);
      normals.push_back(nextSideNorm);

      texturePoints.emplace_back(texture_x_step * (float)(j - 1),
                                 texture_y_step * (float)i);
      texturePoints.emplace_back(texture_x_step * (float)(j - 1),
                                 texture_y_step * (float)(i - 1));
      texturePoints.emplace_back(texture_x_step * (float)(j),
                                 texture_y_step * (float)(i));
      texturePoints.emplace_back(texture_x_step * (float)(j),
                                 texture_y_step * (float)(i - 1));

      no_points += 4;
      no_indexes += 6;

      r.emplace_back(p1, p3, p2, p4, 1);
    }
  }

  return {r, no_points, no_indexes, normals, texturePoints};
}