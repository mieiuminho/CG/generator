#include "Sphere.h"

float Sphere ::getRadius() { return this->radius; }

int Sphere ::getStacks() { return this->stacks; }

int Sphere ::getSlices() { return this->slices; }

Point_2D Sphere ::getTextureCoordinates(int curr_stack, int curr_slice) {

  float texture_x_step = 1.0f / (float)this->slices;
  float texture_y_step = 1.0f / (float)this->stacks;

  float x = texture_x_step * (float)curr_slice;
  float y = texture_y_step * (float)curr_stack;

  return {-x, y};
}

Model Sphere ::generateModel() {

  float stack_step = M_PI / ((float)this->stacks);
  float slice_step = 2 * M_PI / ((float)this->slices);
  float stack_angle, next_stack_angle;
  float slice_angle, next_slice_angle;

  int no_points = 0;
  int no_indexes = 0;

  Point_3D p0, p1, p2, p3;

  float x0, y0, z0;
  float x1, y1, z1;
  float x2, y2, z2;
  float x3, y3, z3;

  vector<SimplePlan> simplePlanes;

  vector<Point_3D> normals;

  vector<Point_2D> texturePoints;

  for (int i = 0; i < this->stacks; i++) {

    stack_angle = (M_PI / 2) - ((float)i) * stack_step;
    next_stack_angle = (M_PI / 2) - (float)(i + 1) * stack_step;

    float zx = this->radius * cosf(stack_angle);
    y1 = this->radius * sinf(stack_angle);
    y3 = y1;

    float zx2 = this->radius * cosf(next_stack_angle);
    y0 = this->radius * sinf(next_stack_angle);
    y2 = y0;

    for (int j = 0; j <= this->slices; j++) {

      slice_angle = (float)(j - 1) * slice_step;
      next_slice_angle = (float)j * slice_step;

      z1 = zx * cosf(slice_angle);
      x1 = zx * sinf(slice_angle);

      z0 = zx2 * cosf(slice_angle);
      x0 = zx2 * sinf(slice_angle);

      z3 = zx * cosf(next_slice_angle);
      x3 = zx * sinf(next_slice_angle);

      z2 = zx2 * cosf(next_slice_angle);
      x2 = zx2 * sinf(next_slice_angle);

      p0 = Point_3D(x0, y0, z0);
      p1 = Point_3D(x1, y1, z1);
      p2 = Point_3D(x2, y2, z2);
      p3 = Point_3D(x3, y3, z3);

      no_points += 4;
      no_indexes += 6;

      texturePoints.push_back(getTextureCoordinates(i, j - 1));
      texturePoints.push_back(getTextureCoordinates(i - 1, j - 1));
      texturePoints.push_back(getTextureCoordinates(i, j));
      texturePoints.push_back(getTextureCoordinates(i - 1, j));

      simplePlanes.emplace_back(p0, p1, p2, p3, 1);

      p0.normalize();
      p1.normalize();
      p2.normalize();
      p3.normalize();

      normals.push_back(p0);
      normals.push_back(p1);
      normals.push_back(p2);
      normals.push_back(p3);
    }
  }

  return {simplePlanes, no_points, no_indexes, normals, texturePoints};
}