#ifndef GENERATOR_RING_H
#define GENERATOR_RING_H
#include "../utils/Model.h"
#include "../utils/Point_3D.h"
#include "../utils/SimplePlan.h"
#include <fstream>
#include <math.h>
#include <sstream>

using namespace std;

class Ring {

private:
  float inner_radius;
  float outter_radius;
  int slices;

public:
  /**
   * Empty constructor.
   */
  Ring() = default;

  /**
   * Parameterized constructor.
   */
  Ring(float inner_radius, float outter_radius, int slices) {
    this->inner_radius = inner_radius;
    this->outter_radius = outter_radius;
    this->slices = slices;
  }

  /**
   * Allows to get the inner radius of the `Ring`.
   */
  float getInnerRadius();

  /**
   * Allows to get the outter radius of the `Ring`.
   */
  float getOutterRadius();

  /**
   * Allows to get the number of slices of the `Ring`.
   */
  int getSlices();

  /**
   * Allows to get the 2D texture coordenates of any point in the sphere.
   */
  Point_2D getTextureCoordinates(int curr_slice, int height);

  /**
   * Allows to generate the `Model` of the `Ring` object.
   * @return The `Ring` object that was created.
   */
  Model generateModel();
};

#endif