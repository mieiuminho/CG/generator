#ifndef GENERATOR_PATCH_H
#define GENERATOR_PATCH_H
#include "../utils/Model.h"
#include "../utils/Point_3D.h"
#include "../utils/SimplePlan.h"
#include <fstream>
#include <math.h>
#include <sstream>
#include <vector>

using namespace std;

class Patch {

private:
  int number_patches;
  vector<vector<int>> patches_indexes;
  int num_control_points;
  vector<Point_3D> control_points;

public:
  Patch() = default;

  Patch(const char *file) {

    ifstream myfile(file);

    string line;

    if (myfile.is_open()) {

      // Get the number of patches
      getline(myfile, line);
      this->number_patches = atoi(line.c_str());

      for (int i = 0; i < this->number_patches; i++) {

        getline(myfile, line);
        this->patches_indexes.push_back(parseIndexes(line.c_str()));
      }

      // Get the number of control points
      getline(myfile, line);
      this->num_control_points = atoi(line.c_str());

      for (int i = 0; i < this->num_control_points; i++) {

        getline(myfile, line);
        float *r = parsePoint(line.c_str());
        Point_3D p = Point_3D(r[0], r[1], r[2]);
        this->control_points.push_back(p);
      }

      myfile.close();
    }
  }

  Patch(int number_patches, vector<vector<int>> patches_indixes,
        int num_control_points, vector<Point_3D> control_points) {
    this->number_patches = number_patches;
    this->patches_indexes = patches_indexes;
    this->num_control_points = num_control_points;
    this->control_points = control_points;
  }

  int getNumberOfPatches() { return this->number_patches; }

  vector<vector<int>> getPatchesIndexes() { return this->patches_indexes; }

  int getNumberOfControlPoints() { return this->number_patches; }

  vector<Point_3D> getControlPoints() { return this->control_points; }

  vector<int> parseIndexes(const char *line);

  float *parsePoint(const char *line);

  vector<Point_3D> get_patch(int index);

  vector<vector<Point_3D>> construct_patch(int index, int tesselation);

  Model constructPatchedModel(int tesselation);

  vector<vector<Point_3D>> construct_patch_normals(int index, int tesselation);

  Point_3D point_at(int patch, int index);

  Point_3D bezier_point(int patch, float du, float dv);

  Point_3D bezier_normal(int patch, float du, float dv);
};

#endif