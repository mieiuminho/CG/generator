#ifndef GENERATOR_BOX_H
#define GENERATOR_BOX_H
#include "Plane.h"

using namespace std;

class Box {

private:
  float x_dimension;
  float y_dimension;
  float z_dimension;
  int divisions;

public:
  /**
   * Empty constructor.
   */
  Box() = default;

  /**
   * Parameterized constructor.
   */
  Box(float x_dimension, float y_dimension, float z_dimension, int divisions) {
    this->x_dimension = x_dimension;
    this->y_dimension = y_dimension;
    this->z_dimension = z_dimension;
    this->divisions = divisions;
  }

  /**
   * Allows to get the _X_ dimension of the `Box` object.
   */
  float getXDimension();

  /**
   * Allows to get the _Y_ dimension of the `Box` object.
   */
  float getYDimension();

  /**
   * Allows to get the _Z_ dimension of the `Box` object.
   */
  float getZDimension();

  /**
   * Allows to generate the `Model` of the `Box` object.
   * @return The `Model` object that was created.
   */
  Model generateModel();
};

#endif