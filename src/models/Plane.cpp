#include "Plane.h"

using namespace std;

Point_3D Plane::get_p1() { return this->p1; }

Point_3D Plane::get_p2() { return this->p2; }

Point_3D Plane::get_p3() { return this->p3; }

Point_3D Plane::get_p4() { return this->p4; }

Plane Plane ::generate_plane_xz(float x, float z) {
  Point_3D p1 = Point_3D(-x / 2, 0, -z / 2);
  Point_3D p2 = Point_3D(-x / 2, 0, z / 2);
  Point_3D p3 = Point_3D(x / 2, 0, -z / 2);
  Point_3D p4 = Point_3D(x / 2, 0, z / 2);
  return {p1, p2, p3, p4};
}

Plane Plane ::generate_plane_yz(float y, float z) {
  Point_3D p1 = Point_3D(0, y / 2, z / 2);
  Point_3D p2 = Point_3D(0, -y / 2, z / 2);
  Point_3D p3 = Point_3D(0, y / 2, -z / 2);
  Point_3D p4 = Point_3D(0, -y / 2, -z / 2);
  return {p1, p2, p3, p4};
}

Plane Plane ::generate_plane_xy(float x, float y) {
  Point_3D p1 = Point_3D(-x / 2, y / 2, 0);
  Point_3D p2 = Point_3D(-x / 2, -y / 2, 0);
  Point_3D p3 = Point_3D(x / 2, y / 2, 0);
  Point_3D p4 = Point_3D(x / 2, -y / 2, 0);
  return {p1, p2, p3, p4};
}

Point_3D Plane ::generate_normal_xz_screenwards() { return {0, 1, 0}; }

Point_3D Plane ::generate_normal_yz_screenwards() { return {1, 0, 0}; }

Point_3D Plane ::generate_normal_xy_screenwards() { return {0, 0, 1}; }

Point_3D Plane ::generate_normal_xz_awaywards() { return {0, -1, 0}; }

Point_3D Plane ::generate_normal_yz_awaywards() { return {-1, 0, 0}; }

Point_3D Plane ::generate_normal_xy_awaywards() { return {0, 0, -1}; }

Point_2D Plane ::getTextureCoordinates(int width, int height) {

  return {(float)width, (float)height};
}

Model Plane ::generateModel() {

  float side_size = 1;

  Plane p = generate_plane_xz(side_size, side_size);

  pair<vector<SimplePlan>, vector<Point_2D>> plansNtex =
      p.generate_upwards_plane_xz(1);

  vector<SimplePlan> points;
  vector<Point_3D> normals;
  vector<Point_2D> texturePoints;
  Point_3D normal = generate_normal_xz_screenwards();
  Point_3D down_normal = generate_normal_xz_awaywards();

  for (int i = 0; i < plansNtex.first.size(); i++) {
    printf("%d\n", i);
    points.push_back(plansNtex.first.at(i));
  }

  for (int i = 0; i < plansNtex.second.size(); i++)
    texturePoints.push_back(plansNtex.second.at(i));

  for (int i = 0; i < 4; i++)
    normals.push_back(normal);

  return {points, 4, 6, normals, texturePoints};
}

void Plane ::shift_plane_yz_along_x(float x) {
  this->p1.setX(this->p1.getX() + x);
  this->p2.setX(this->p2.getX() + x);
  this->p3.setX(this->p3.getX() + x);
  this->p4.setX(this->p4.getX() + x);
}

void Plane ::shift_plane_xz_along_y(float y) {
  this->p1.setY(this->p1.getY() + y);
  this->p2.setY(this->p2.getY() + y);
  this->p3.setY(this->p3.getY() + y);
  this->p4.setY(this->p4.getY() + y);
}

void Plane ::shift_plane_xy_along_z(float z) {
  this->p1.setZ(this->p1.getZ() + z);
  this->p2.setZ(this->p2.getZ() + z);
  this->p3.setZ(this->p3.getZ() + z);
  this->p4.setZ(this->p4.getZ() + z);
}

pair<vector<SimplePlan>, vector<Point_2D>>
Plane ::generate_screenwards_plane_xy(int divisions) {

  float x_length = p4.getX() - p1.getX();
  float y_length = p1.getY() - p2.getY();

  float x_step = x_length * (1 / (float)divisions);
  float y_step = y_length * (1 / (float)divisions);

  float x1 = this->p1.getX();
  float y1 = this->p1.getY();
  float z1 = this->p1.getZ();

  vector<SimplePlan> r;
  vector<Point_2D> texturePoints;

  float texture_x_step = 1.0f / (float)divisions;
  float texture_y_step = 1.0f / (float)divisions;

  for (int j = 0; j < divisions; j++) {

    for (int i = 0; i < divisions; i++) {

      Point_3D zero = {x1 + (float(i) * x_step), y1 - (float(j) * y_step), z1};

      Point_3D one = {x1 + (float(i) * x_step), y1 - (float(j + 1) * y_step),
                      z1};

      Point_3D two = {x1 + (float(i + 1) * x_step), y1 - (float(j) * y_step),
                      z1};

      Point_3D three = {x1 + (float(i + 1) * x_step),
                        y1 - (float(j + 1) * y_step), z1};

      Point_2D zeroTex = {(float(i)) * texture_x_step,
                          1 - (float(j)) * texture_y_step};

      Point_2D oneTex = {(float(i)) * texture_x_step,
                         1 - (float(j + 1)) * texture_y_step};

      Point_2D twoTex = {(float(i + 1)) * texture_x_step,
                         1 - (float(j)) * texture_y_step};

      Point_2D threeTex = {(float(i + 1)) * texture_x_step,
                           1 - (float(j + 1)) * texture_y_step};

      r.emplace_back(zero, one, two, three, 1);

      texturePoints.push_back(zeroTex);
      texturePoints.push_back(oneTex);
      texturePoints.push_back(twoTex);
      texturePoints.push_back(threeTex);
    }
  }

  return {r, texturePoints};
}

pair<vector<SimplePlan>, vector<Point_2D>>
Plane ::generate_awaywards_plane_xy(int divisions) {

  float x_length = p4.getX() - p1.getX();
  float y_length = p1.getY() - p2.getY();

  float x_step = x_length * (1 / (float)divisions);
  float y_step = y_length * (1 / (float)divisions);

  float x1 = this->p1.getX();
  float y1 = this->p1.getY();
  float z1 = this->p1.getZ();

  vector<SimplePlan> r;

  vector<Point_2D> texturePoints;

  float texture_x_step = 1.0f / (float)divisions;
  float texture_y_step = 1.0f / (float)divisions;

  for (int j = 0; j < divisions; j++) {

    for (int i = 0; i < divisions; i++) {

      Point_3D zero = {x1 + (float(i) * x_step), y1 - (float(j) * y_step), z1};

      Point_3D one = {x1 + (float(i) * x_step), y1 - (float(j + 1) * y_step),
                      z1};

      Point_3D two = {x1 + (float(i + 1) * x_step), y1 - (float(j) * y_step),
                      z1};

      Point_3D three = {x1 + (float(i + 1) * x_step),
                        y1 - (float(j + 1) * y_step), z1};

      Point_2D zeroTex = {(float(i)) * texture_x_step,
                          1 - (float(j)) * texture_y_step};

      Point_2D oneTex = {(float(i)) * texture_x_step,
                         1 - (float(j + 1)) * texture_y_step};

      Point_2D twoTex = {(float(i + 1)) * texture_x_step,
                         1 - (float(j)) * texture_y_step};

      Point_2D threeTex = {(float(i + 1)) * texture_x_step,
                           1 - (float(j + 1)) * texture_y_step};

      r.emplace_back(zero, one, two, three, -1);

      texturePoints.push_back(zeroTex);
      texturePoints.push_back(oneTex);
      texturePoints.push_back(twoTex);
      texturePoints.push_back(threeTex);
    }
  }

  return {r, texturePoints};
}

pair<vector<SimplePlan>, vector<Point_2D>>
Plane ::generate_upwards_plane_xz(int divisions) {

  float x_length = p4.getX() - p1.getX();
  float z_length = p2.getZ() - p1.getZ();

  float x_step = x_length * (1 / (float)divisions);
  float z_step = z_length * (1 / (float)divisions);

  float x1 = p1.getX();
  float y1 = p1.getY();
  float z1 = p1.getZ();

  vector<SimplePlan> r;
  vector<Point_2D> texturePoints;

  float texture_x_step = 1.0f / (float)divisions;
  float texture_y_step = 1.0f / (float)divisions;

  for (int j = 0; j < divisions; j++) {

    for (int i = 0; i < divisions; i++) {

      Point_3D zero = {x1 + (float(i) * x_step), y1, z1 + (float(j) * z_step)};

      Point_3D one = {x1 + (float(i) * x_step), y1,
                      z1 + (float(j + 1) * z_step)};

      Point_3D two = {x1 + (float(i + 1) * x_step), y1,
                      z1 + (float(j) * z_step)};

      Point_3D three = {x1 + (float(i + 1) * x_step), y1,
                        z1 + (float(j + 1) * z_step)};

      Point_2D zeroTex = {(float(i)) * texture_x_step,
                          (float(j)) * texture_y_step};

      Point_2D oneTex = {(float(i)) * texture_x_step,
                         (float(j + 1)) * texture_y_step};

      Point_2D twoTex = {(float(i + 1)) * texture_x_step,
                         (float(j)) * texture_y_step};

      Point_2D threeTex = {(float(i + 1)) * texture_x_step,
                           (float(j + 1)) * texture_y_step};

      r.emplace_back(zero, one, two, three, 1);

      texturePoints.push_back(zeroTex);
      texturePoints.push_back(oneTex);
      texturePoints.push_back(twoTex);
      texturePoints.push_back(threeTex);
    }
  }

  return {r, texturePoints};
}

pair<vector<SimplePlan>, vector<Point_2D>>
Plane ::generate_downwards_plane_xz(int divisions) {

  float x_length = p4.getX() - p1.getX();
  float z_length = p2.getZ() - p1.getZ();

  float x_step = x_length * (1 / (float)divisions);
  float z_step = z_length * (1 / (float)divisions);

  float x1 = p1.getX();
  float y1 = p1.getY();
  float z1 = p1.getZ();

  vector<SimplePlan> r;
  vector<Point_2D> texturePoints;

  float texture_x_step = 1.0f / (float)divisions;
  float texture_y_step = 1.0f / (float)divisions;

  for (int j = 0; j < divisions; j++) {

    for (int i = 0; i < divisions; i++) {

      Point_3D zero = {x1 + (float(i) * x_step), y1, z1 + (float(j) * z_step)};

      Point_3D one = {x1 + (float(i) * x_step), y1,
                      z1 + (float(j + 1) * z_step)};

      Point_3D two = {x1 + (float(i + 1) * x_step), y1,
                      z1 + (float(j) * z_step)};

      Point_3D three = {x1 + (float(i + 1) * x_step), y1,
                        z1 + (float(j + 1) * z_step)};

      Point_2D zeroTex = {(float(i)) * texture_x_step,
                          (float(j)) * texture_y_step};

      Point_2D oneTex = {(float(i)) * texture_x_step,
                         (float(j + 1)) * texture_y_step};

      Point_2D twoTex = {(float(i + 1)) * texture_x_step,
                         (float(j)) * texture_y_step};

      Point_2D threeTex = {(float(i + 1)) * texture_x_step,
                           (float(j + 1)) * texture_y_step};

      r.emplace_back(zero, one, two, three, -1);

      texturePoints.push_back(zeroTex);
      texturePoints.push_back(oneTex);
      texturePoints.push_back(twoTex);
      texturePoints.push_back(threeTex);
    }
  }

  return {r, texturePoints};
}

pair<vector<SimplePlan>, vector<Point_2D>>
Plane ::generate_rightwards_plane_yz(int divisions) {

  float y_length = p1.getY() - p2.getY();
  float z_length = p2.getZ() - p3.getZ();

  float y_step = y_length * (1 / (float)divisions);
  float z_step = z_length * (1 / (float)divisions);

  float x1 = p1.getX();
  float y1 = p1.getY();
  float z1 = p1.getZ();

  vector<SimplePlan> r;
  vector<Point_2D> texturePoints;

  float texture_x_step = 1.0f / (float)divisions;
  float texture_y_step = 1.0f / (float)divisions;

  for (int j = 0; j < divisions; j++) {

    for (int i = 0; i < divisions; i++) {

      Point_3D zero = {x1, y1 - (float(i) * y_step), z1 - (float(j) * z_step)};

      Point_3D one = {x1, y1 - (float(i + 1) * y_step),
                      z1 - (float(j) * z_step)};

      Point_3D two = {x1, y1 - (float(i) * y_step),
                      z1 - (float(j + 1) * z_step)};

      Point_3D three = {x1, y1 - (float(i + 1) * y_step),
                        z1 - (float(j + 1) * z_step)};

      Point_2D zeroTex = {1 - (float(i)) * texture_x_step,
                          1 - (float(j)) * texture_y_step};

      Point_2D oneTex = {1 - (float(i + 1)) * texture_x_step,
                         1 - (float(j)) * texture_y_step};

      Point_2D twoTex = {1 - (float(i)) * texture_x_step,
                         1 - (float(j + 1)) * texture_y_step};

      Point_2D threeTex = {1 - (float(i + 1)) * texture_x_step,
                           1 - (float(j + 1)) * texture_y_step};

      r.emplace_back(zero, one, two, three, 1);

      texturePoints.push_back(zeroTex);
      texturePoints.push_back(oneTex);
      texturePoints.push_back(twoTex);
      texturePoints.push_back(threeTex);
    }
  }

  return {r, texturePoints};
}

pair<vector<SimplePlan>, vector<Point_2D>>
Plane ::generate_leftwards_plane_yz(int divisions) {

  float y_length = p1.getY() - p2.getY();
  float z_length = p2.getZ() - p3.getZ();

  float y_step = y_length * (1 / (float)divisions);
  float z_step = z_length * (1 / (float)divisions);

  float x1 = p1.getX();
  float y1 = p1.getY();
  float z1 = p1.getZ();

  vector<SimplePlan> r;
  vector<Point_2D> texturePoints;

  float texture_x_step = 1.0f / (float)divisions;
  float texture_y_step = 1.0f / (float)divisions;

  for (int j = 0; j < divisions; j++) {

    for (int i = 0; i < divisions; i++) {

      Point_3D zero = {x1, y1 - (float(i) * y_step), z1 - (float(j) * z_step)};

      Point_3D one = {x1, y1 - (float(i + 1) * y_step),
                      z1 - (float(j) * z_step)};

      Point_3D two = {x1, y1 - (float(i) * y_step),
                      z1 - (float(j + 1) * z_step)};

      Point_3D three = {x1, y1 - (float(i + 1) * y_step),
                        z1 - (float(j + 1) * z_step)};

      Point_2D zeroTex = {1 - (float(i)) * texture_x_step,
                          1 - (float(j)) * texture_y_step};

      Point_2D oneTex = {1 - (float(i + 1)) * texture_x_step,
                         1 - (float(j)) * texture_y_step};

      Point_2D twoTex = {1 - (float(i)) * texture_x_step,
                         1 - (float(j + 1)) * texture_y_step};

      Point_2D threeTex = {1 - (float(i + 1)) * texture_x_step,
                           1 - (float(j + 1)) * texture_y_step};

      r.emplace_back(zero, one, two, three, -1);

      texturePoints.push_back(zeroTex);
      texturePoints.push_back(oneTex);
      texturePoints.push_back(twoTex);
      texturePoints.push_back(threeTex);
    }
  }

  return {r, texturePoints};
}
