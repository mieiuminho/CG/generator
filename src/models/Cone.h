#ifndef GENERATOR_CONE_H
#define GENERATOR_CONE_H
#include "../utils/Model.h"
#include "../utils/Point_3D.h"
#include "../utils/SimplePlan.h"
#include <fstream>
#include <math.h>
#include <sstream>

using namespace std;

class Cone {

private:
  float bottom_radius;
  float height;
  int stacks;
  int slices;

public:
  /**
   * Empty constructor.
   */
  Cone() = default;

  /**
   * Parameterized constructor.
   * @param bottom_radius Bottom radius of the `Cone`.
   * @param height Height of the `Cone`.
   * @param stack Number of stacks of the `Cone`.
   * @param slices Number of slices of the `Cone`.
   */
  Cone(float bottom_radius, float height, int stacks, int slices) {
    this->bottom_radius = bottom_radius;
    this->height = height;
    this->stacks = stacks;
    this->slices = slices;
  }

  /**
   * Allows to get the bottom radius of the `Cone`.
   */
  float getBottomRadius();

  /**
   * Allows to get the height of the `Cone`.
   */
  float getHeight();

  /**
   * Allows to get the stacks of the `Cone`.
   */
  int getStacks();

  /**
   * Allows to get the slices of the `Cone`.
   */
  int getSlices();

  /**
   * Allows to generate the `Model` of the `Cone` object.
   * @return The `Model` object that was created.
   */
  Model generateModel();
};

#endif