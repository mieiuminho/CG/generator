#ifndef GENERATOR_SPHERE_H
#define GENERATOR_SPHERE_H
#include "../utils/Model.h"
#include "../utils/Point_3D.h"
#include "../utils/SimplePlan.h"
#include <fstream>
#include <math.h>
#include <sstream>
#include <vector>

using namespace std;

class Sphere {

private:
  float radius;
  int stacks;
  int slices;

public:
  /**
   * Empty constructor.
   */
  Sphere() = default;

  /**
   * Parameterized constructor.
   * @param radius Sphere's radius.
   * @param stacks Sphere's number of stacks.
   * @param slices Sphere's number of slices.
   */
  Sphere(float radius, int stacks, int slices) {
    this->radius = radius;
    this->stacks = stacks;
    this->slices = slices;
  }

  /**
   * Allows to get the radius of the `Sphere` object.
   */
  float getRadius();

  /**
   * Allows to get the number of stacks of the `Sphere` object.
   */
  int getStacks();

  /**
   * Allows to get the number of slices of the `Sphere` object.
   */
  int getSlices();

  /**
   * Allows to get the 2D textures coordenates of any point in the sphere.
   */
  Point_2D getTextureCoordinates(int curr_stack, int curr_slice);

  /**
   * Allows to generate the `Model` of the `Sphere` object.
   * @return The `Sphere` object that was created.
   */
  Model generateModel();
};

#endif