#include "Ring.h"

float Ring ::getInnerRadius() { return this->inner_radius; }

float Ring ::getOutterRadius() { return this->outter_radius; }

int Ring ::getSlices() { return this->slices; }

Point_2D Ring ::getTextureCoordinates(int curr_slice, int height) {

  float texture_x_step = 1.0f / (float)this->slices;

  float x = texture_x_step * (float)curr_slice;

  return {x, (float)height};
}

Model Ring ::generateModel() {

  float ang_step = (float)(2 * M_PI) * (1 / ((float)this->slices));
  float slice_angle, next_slice_angle;

  float x0, y0, z0;
  float x1, y1, z1;
  float x2, y2, z2;
  float x3, y3, z3;

  Point_3D p0, p1, p2, p3;

  vector<SimplePlan> simplePlans;
  vector<Point_3D> normals;
  vector<Point_2D> texturePoints;

  int no_points = 0;
  int no_indexes = 0;

  for (int i = 0; i <= this->slices; i++) {

    slice_angle = ang_step * i;
    next_slice_angle = ang_step * (i + 1);

    y0 = 0;
    x0 = sinf(slice_angle) * this->inner_radius;
    z0 = cosf(slice_angle) * this->inner_radius;

    y1 = 0;
    x1 = sinf(slice_angle) * this->outter_radius;
    z1 = cosf(slice_angle) * this->outter_radius;

    y3 = 0;
    x3 = sinf(next_slice_angle) * this->outter_radius;
    z3 = cosf(next_slice_angle) * this->outter_radius;

    y2 = 0;
    x2 = sinf(next_slice_angle) * this->inner_radius;
    z2 = cosf(next_slice_angle) * this->inner_radius;

    p0 = Point_3D(x0, y0, z0);
    p1 = Point_3D(x1, y1, z1);
    p2 = Point_3D(x2, y2, z2);
    p3 = Point_3D(x3, y3, z3);

    no_points += 8;
    no_indexes += 12;

    // Normais do "quadrado" que aponta para cima
    normals.emplace_back(0, 1, 0);
    normals.emplace_back(0, 1, 0);
    normals.emplace_back(0, 1, 0);
    normals.emplace_back(0, 1, 0);

    // Normais do "quadrado" que aponta para baixo
    normals.emplace_back(0, -1, 0);
    normals.emplace_back(0, -1, 0);
    normals.emplace_back(0, -1, 0);
    normals.emplace_back(0, -1, 0);

    texturePoints.emplace_back(getTextureCoordinates(i, 1));
    texturePoints.emplace_back(getTextureCoordinates(i, 0));
    texturePoints.emplace_back(getTextureCoordinates(i + 1, 1));
    texturePoints.emplace_back(getTextureCoordinates(i + 1, 0));
    texturePoints.emplace_back(getTextureCoordinates(i, 1));
    texturePoints.emplace_back(getTextureCoordinates(i, 0));
    texturePoints.emplace_back(getTextureCoordinates(i + 1, 1));
    texturePoints.emplace_back(getTextureCoordinates(i + 1, 0));

    simplePlans.emplace_back(p0, p1, p2, p3, 1);
    simplePlans.emplace_back(p0, p1, p2, p3, -1);
  }

  return {simplePlans, no_points, no_indexes, normals, texturePoints};
}