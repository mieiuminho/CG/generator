#ifndef GENERATOR_PLANE_H
#define GENERATOR_PLANE_H
#include "../utils/Model.h"
#include "../utils/Point_3D.h"
#include "../utils/SimplePlan.h"
#include <fstream>
#include <iostream>

using namespace std;

class Plane {

private:
  Point_3D p1;
  Point_3D p2;
  Point_3D p3;
  Point_3D p4;

public:
  /**
   * Empty constructor.
   */
  Plane() {}

  /**
   * Parameterized constructor.
   * @param p1 Point of the plane.
   * @param p2 Point of the plane.
   * @param p3 Point of the plane.
   * @param p4 Point of the plane.
   */
  Plane(Point_3D p1, Point_3D p2, Point_3D p3, Point_3D p4) {
    this->p1 = p1;
    this->p2 = p2;
    this->p3 = p3;
    this->p4 = p4;
  }

  /**
   * Allows to get the first point of the plane.
   * @return `Point_3D` object.
   */
  Point_3D get_p1();

  /**
   * Allows to get the second point of the plane.
   * @return `Point_3D` object.
   */
  Point_3D get_p2();

  /**
   *  Allows to get the third point of the plane.
   *  @return `Point_3D` object.
   */
  Point_3D get_p3();

  /**
   * Allows to get the fourth point of the plane.
   * @return `Point_3D` object.
   */
  Point_3D get_p4();

  /**
   * Allows to generate a `Plane` object (that is a _xz_ oriented plane).
   * @param x _X_ dimension of the plane.
   * @param z _Z_ dimension of the plane.
   * @return The `Plane` object.
   */
  static Plane generate_plane_xz(float x, float z);

  /**
   * Allows to generate a `Plane` object (that is a _yz_ oriented plane).
   * @param y _Y_ dimension of the plane.
   * @param z _Z_ dimension of the plane.
   * @return The `Plane` object.
   */
  static Plane generate_plane_yz(float y, float z);

  /**
   * Allows to generate a `Plane` object (that is a _xy_ oriented plane).
   * @param x _X_ dimension of the plane.
   * @param y _Y_ dimension of the plane.
   * @return The `Plane` object.
   */
  static Plane generate_plane_xy(float x, float y);

  /**
   * Allows to get the texture coordenates of a plane.
   * @return Point_2D object.
   */
  Point_2D getTextureCoordinates(int width, int height);

  /**
   * Allows to generate a model of the plane object.
   * @return The `Model` object that was created.
   */
  Model generateModel();

  /**
   * Allows to move a plane along the _x_ axis.
   * @param x Amount of units that the plane should be moved along the plane.
   */
  void shift_plane_yz_along_x(float x);

  /**
   * Allows to move a plane along the _y_ axis.
   * @param y Amount of units that the plane should be moved along the plane.
   */
  void shift_plane_xz_along_y(float y);

  /**
   * Allows to move a plane along the _z_ axis.
   * @param z Amount of units that the plane should be moved along the plane.
   */
  void shift_plane_xy_along_z(float z);

  /**
   * Allows to generate a _yz_ oriented plane (with **rightwards** orientation).
   * @param divisions Number of the divisions of the plane.
   * @return A vector of `SimplePlan` objects that add up to the whole plan.
   */
  pair<vector<SimplePlan>, vector<Point_2D>>
  generate_rightwards_plane_yz(int divisions);

  /**
   * Allows to generate a _yz_ oriented plane (with **leftwards** orientation).
   * @param divisions Number of the divisions of the plane.
   * @return A vector of `SimplePlan` objects that add up to the whole plan.
   */
  pair<vector<SimplePlan>, vector<Point_2D>>
  generate_leftwards_plane_yz(int divisions);

  /**
   * Allows to generate a _xz_ oriented plane (with **downwards** orientation).
   * @param divisions  Number of the divions of the plane.
   * @return A vector of `SimplePlan` objects that add up to the whole plan.
   */
  pair<vector<SimplePlan>, vector<Point_2D>>
  generate_downwards_plane_xz(int divisions);

  /**
   * Allows to generate a _xz_ oriented plane (with **upwards** orientation).
   * @param divisions Number of the divisions of the plane.
   * @return A vector of `SimplePlan` objects that add up to the whole plan.
   */
  pair<vector<SimplePlan>, vector<Point_2D>>
  generate_upwards_plane_xz(int divisions);

  /**
   * Allows to generate a _xy_ oriented plane (with **screenwards**
   * orientation).
   * @param divisions  Number of the divisions of the plane.
   * @return A vector of `SimplePlan` objects that add up to the whole plan.
   */
  pair<vector<SimplePlan>, vector<Point_2D>>
  generate_screenwards_plane_xy(int divisions);

  /**
   * Allows to generate a _xy_ oriented plane (with **awaywards** orientation).
   * @param divisions Number of the divisions of the plane.
   * @return A vector of `SimplePlan` objects that add up to the whole plan.
   */
  pair<vector<SimplePlan>, vector<Point_2D>>
  generate_awaywards_plane_xy(int divisions);

  Point_3D generate_normal_xz_screenwards();

  Point_3D generate_normal_yz_screenwards();

  Point_3D generate_normal_xy_screenwards();

  Point_3D generate_normal_xz_awaywards();

  Point_3D generate_normal_yz_awaywards();

  Point_3D generate_normal_xy_awaywards();
};

#endif
