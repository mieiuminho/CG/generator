#include "Box.h"

float Box ::getXDimension() { return this->x_dimension; }

float Box ::getYDimension() { return this->y_dimension; }

float Box ::getZDimension() { return this->z_dimension; }

Model Box ::generateModel() {

  vector<SimplePlan> r;
  vector<Point_3D> normals;
  vector<Point_2D> texturePoints;
  int squares_per_face = this->divisions * this->divisions;

  Plane xy = Plane::generate_plane_xy(this->x_dimension, this->y_dimension);
  Plane yz = Plane::generate_plane_yz(this->y_dimension, this->z_dimension);
  Plane xz = Plane::generate_plane_xz(this->x_dimension, this->z_dimension);

  xy.shift_plane_xy_along_z(this->z_dimension / 2);
  // Front
  pair<vector<SimplePlan>, vector<Point_2D>> face1 =
      xy.generate_screenwards_plane_xy(divisions);

  // Back
  xy.shift_plane_xy_along_z(-this->z_dimension);
  pair<vector<SimplePlan>, vector<Point_2D>> face2 =
      xy.generate_awaywards_plane_xy(divisions);

  yz.shift_plane_yz_along_x(this->x_dimension / 2);
  // Right
  pair<vector<SimplePlan>, vector<Point_2D>> face3 =
      yz.generate_rightwards_plane_yz(divisions);

  yz.shift_plane_yz_along_x(-this->x_dimension);
  // Left
  pair<vector<SimplePlan>, vector<Point_2D>> face4 =
      yz.generate_leftwards_plane_yz(divisions);

  xz.shift_plane_xz_along_y(this->y_dimension / 2);
  // Top
  pair<vector<SimplePlan>, vector<Point_2D>> face5 =
      xz.generate_upwards_plane_xz(divisions);

  xz.shift_plane_xz_along_y(-this->y_dimension);
  // Bottom
  pair<vector<SimplePlan>, vector<Point_2D>> face6 =
      xz.generate_downwards_plane_xz(divisions);

  int limit = this->divisions * this->divisions;

  // Não vão para o vector mais pontos do que aqueles que estamos à espera
  r.reserve(limit * 6);

  for (int i = 0; i < squares_per_face * 4; i++) {
    normals.push_back({0, 0, 1});
  }
  for (int i = 0; i < squares_per_face * 4; i++) {
    normals.push_back({0, 1, 0});
  }
  for (int i = 0; i < squares_per_face * 4; i++) {
    normals.push_back({-1, 0, 0});
  }
  for (int i = 0; i < squares_per_face * 4; i++) {
    normals.push_back({0, 0, -1});
  }
  for (int i = 0; i < squares_per_face * 4; i++) {
    normals.push_back({1, 0, 0});
  }
  for (int i = 0; i < squares_per_face * 4; i++) {
    normals.push_back({0, -1, 0});
  }
  for (int i = 0; i < limit; i++) {
    r.push_back(face1.first.at(i));
  }
  for (int i = 0; i < limit * 4; i++) {
    texturePoints.push_back(face1.second.at(i));
  }
  for (int i = 0; i < limit; i++) {
    r.push_back(face5.first.at(i));
  }
  for (int i = 0; i < limit * 4; i++) {
    texturePoints.push_back(face5.second.at(i));
  }
  for (int i = 0; i < limit; i++) {
    r.push_back(face4.first.at(i));
  }
  for (int i = 0; i < limit * 4; i++) {
    texturePoints.push_back(face4.second.at(i));
  }
  for (int i = 0; i < limit; i++) {
    r.push_back(face2.first.at(i));
  }
  for (int i = 0; i < limit * 4; i++) {
    texturePoints.push_back(face2.second.at(i));
  }
  for (int i = 0; i < limit; i++) {
    r.push_back(face3.first.at(i));
  }
  for (int i = 0; i < limit * 4; i++) {
    texturePoints.push_back(face3.second.at(i));
  }
  for (int i = 0; i < limit; i++) {
    r.push_back(face6.first.at(i));
  }
  for (int i = 0; i < limit * 4; i++) {
    texturePoints.push_back(face6.second.at(i));
  }

  return {r, 24 * this->divisions * this->divisions,
          36 * this->divisions * this->divisions, normals, texturePoints};
}