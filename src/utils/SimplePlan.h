#ifndef GENERATOR_SIMPLEPLAN_H
#define GENERATOR_SIMPLEPLAN_H
#include "Point_3D.h"
#include <fstream>
#include <iosfwd>
#include <vector>

using namespace std;

class SimplePlan {

private:
  vector<Point_3D> points;
  int orientation;

public:
  SimplePlan(Point_3D p1, Point_3D p2, Point_3D p3, Point_3D p4,
             int orientation) {
    this->points.push_back(p1);
    this->points.push_back(p2);
    this->points.push_back(p3);
    this->points.push_back(p4);
    this->orientation = orientation;
  }

  SimplePlan(Point_3D p1, Point_3D p2, Point_3D p3, int orientation) {
    this->points.push_back(p1);
    this->points.push_back(p2);
    this->points.push_back(p3);
    this->orientation = orientation;
  }

  void write_points_to_file(char *filename);

  void write_indexes_to_file(char *filename, int square_no);

  int isTriangle();
};

#endif // GENERATOR_SIMPLEPLAN_H
