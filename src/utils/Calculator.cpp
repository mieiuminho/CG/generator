#include "Calculator.h"
#include <cmath>

int Calculator ::factorial(int n) {
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

float Calculator ::combinations(int degree, int i) {
  auto denominator = float(factorial(i) * factorial(degree - i));
  return (float)(factorial(degree) / denominator);
}

float Calculator ::bernstein(int i, int degree, float u) {
  float c = pow(u, i) * pow(1 - u, degree - i);
  return combinations(degree, i) * c;
}

void Calculator ::vector_by_matrix_mul(float v[4], float m[4][4],
                                       float res[4]) {

  for (int i = 0; i < 4; i++) {
    res[i] = 0;
    for (int j = 0; j < 4; j++) {
      res[i] += v[j] * m[j][i];
    }
  }
}

void Calculator ::vector_by_point_matrix(float v[4], Point_3D m[4][4],
                                         Point_3D res[4]) {

  for (int i = 0; i < 4; i++) {
    res[i] = new Point_3D();
    res[i].setX(0);
    res[i].setY(0);
    res[i].setZ(0);
    for (int j = 0; j < 4; j++) {
      res[i].setX(res[i].getX() + v[j] * m[j][i].getX());
      res[i].setY(res[i].getY() + v[j] * m[j][i].getY());
      res[i].setZ(res[i].getZ() + v[j] * m[j][i].getZ());
    }
  }
}

void Calculator ::point_vector_by_matrix(Point_3D v[4], float m[4][4],
                                         Point_3D res[4]) {

  for (int i = 0; i < 4; i++) {
    res[i] = new Point_3D();
    res[i].setX(0);
    res[i].setY(0);
    res[i].setZ(0);
    for (int j = 0; j < 4; j++) {
      res[i].setX(res[i].getX() + v[j].getX() * m[j][i]);
      res[i].setY(res[i].getY() + v[j].getY() * m[j][i]);
      res[i].setZ(res[i].getZ() + v[j].getZ() * m[j][i]);
    }
  }
}

Point_3D Calculator ::point_vector_by_vector(Point_3D v[4], float v2[4]) {

  Point_3D p = new Point_3D();
  p.setX(0);
  p.setY(0);
  p.setZ(0);
  for (int i = 0; i < 4; i++) {
    p.setX(p.getX() + v[i].getX() * v2[i]);
    p.setY(p.getY() + v[i].getY() * v2[i]);
    p.setZ(p.getZ() + v[i].getZ() * v2[i]);
  }

  return p;
}
