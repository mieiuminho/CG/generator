#include "Model.h"

void Model ::write_model_to_file(char *filename) {

  ofstream file;

  file.open(filename);

  file << "POINTS\n";

  file << this->no_points << "\n";

  file.close();

  for (int i = 0; i < this->plans.size(); i++) {

    this->plans.at(i).write_points_to_file(filename);
  }

  file.open(filename, ofstream::app);

  file << "INDEXES\n";

  file << this->no_indexes << "\n";

  file.close();

  int no_square = 0;

  for (int i = 0; i < this->plans.size(); i++) {

    this->plans.at(i).write_indexes_to_file(filename, no_square);
    if (this->plans.at(i).isTriangle())
      no_square += 3;
    else
      no_square += 4;
  }

  file.open(filename, ofstream::app);

  file << "NORMALS\n";

  file << this->normals.size() << "\n";

  for (int i = 0; i < this->normals.size(); i++) {

    file << this->normals.at(i).toString();
  }

  file.close();

  file.open(filename, ofstream::app);

  file << "TEXTURE POINTS\n";

  file << this->texturePoints.size() << "\n";

  for (int i = 0; i < this->texturePoints.size(); i++) {

    file << this->texturePoints.at(i).toString();
  }

  file.close();
}
