#include "Point_2D.h"

float Point_2D ::getX() { return this->x; }

float Point_2D ::getY() { return this->y; }

void Point_2D ::setX(float x) { this->x = x; }

void Point_2D ::setY(float y) { this->y = y; }

void Point_2D ::setCoordenates(float x, float y) {
  this->x = x;
  this->y = y;
}

char *Point_2D ::toString() {
  char *r = (char *)malloc(50);
  sprintf(r, "%f, %f\n", this->x, this->y);
  return r;
}
