#ifndef GENERATOR_CALCULATOR_H
#define GENERATOR_CALCULATOR_H

#include "Point_3D.h"

class Calculator {

public:
  static int factorial(int n);

  static float combinations(int degree, int i);

  static float bernstein(int i, int degree, float u);

  static void vector_by_matrix_mul(float v[4], float m[4][4], float res[4]);

  static void vector_by_point_matrix(float v[4], Point_3D m[4][4],
                                     Point_3D res[4]);

  static void point_vector_by_matrix(Point_3D v[4], float m[4][4],
                                     Point_3D res[4]);

  static Point_3D point_vector_by_vector(Point_3D v[4], float v2[4]);
};

#endif
