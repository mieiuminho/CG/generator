#include "SimplePlan.h"

void SimplePlan ::write_points_to_file(char *filename) {

  ofstream file;

  file.open(filename, ofstream::app);

  if (this->points.size() == 3) {
    file << this->points.at(0).toString();
    file << this->points.at(1).toString();
    file << this->points.at(2).toString();
  } else {
    file << this->points.at(0).toString();
    file << this->points.at(1).toString();
    file << this->points.at(2).toString();
    file << this->points.at(3).toString();
  }

  file.close();
}

void SimplePlan ::write_indexes_to_file(char *filename, int square_no) {

  ofstream file;

  file.open(filename, ofstream::app);

  if (this->points.size() == 3) {
    if (this->orientation == 1) {
      file << square_no << "\n";
      file << square_no + 1 << "\n";
      file << square_no + 2 << "\n";
    } else {
      file << square_no << "\n";
      file << square_no + 2 << "\n";
      file << square_no + 1 << "\n";
    }
  } else {
    if (this->orientation == 1) {
      file << square_no << "\n";
      file << square_no + 1 << "\n";
      file << square_no + 2 << "\n";
      file << square_no + 2 << "\n";
      file << square_no + 1 << "\n";
      file << square_no + 3 << "\n";
    } else {
      file << square_no << "\n";
      file << square_no + 2 << "\n";
      file << square_no + 1 << "\n";
      file << square_no + 2 << "\n";
      file << square_no + 3 << "\n";
      file << square_no + 1 << "\n";
    }
  }

  file.close();
}

int SimplePlan ::isTriangle() {
  if (this->points.size() == 3)
    return 1;
  else
    return 0;
}
