#ifndef GENERATOR_MODEL_H
#define GENERATOR_MODEL_H
#include "Point_2D.h"
#include "SimplePlan.h"
#include <vector>

class Model {

private:
  vector<SimplePlan> plans;
  int no_points;
  int no_indexes;
  vector<Point_3D> normals;
  vector<Point_2D> texturePoints;

public:
  Model() {}

  Model(vector<SimplePlan> plans, int no_points, int no_indexes,
        vector<Point_3D> normals, vector<Point_2D> texturePoints) {
    this->plans = plans;
    this->no_points = no_points;
    this->no_indexes = no_indexes;
    this->normals = normals;
    this->texturePoints = texturePoints;
  }

  void write_model_to_file(char *filename);
};

#endif
