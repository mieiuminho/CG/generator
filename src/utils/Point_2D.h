#ifndef GENERATOR_POINT_2D_H
#define GENERATOR_POINT_2D_H
#include <cstdio>
#include <cstdlib>

class Point_2D {

private:
  float x;
  float y;

public:
  Point_2D() = default;

  Point_2D(float x, float y) {
    this->x = x;
    this->y = y;
  }

  Point_2D(Point_2D *p) {
    this->x = p->x;
    this->y = p->y;
  }

  float getX();

  float getY();

  void setX(float x);

  void setY(float y);

  void setCoordenates(float x, float y);

  char *toString();
};

#endif // GENERATOR_POINT_2D_H
