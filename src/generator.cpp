#include "generator.h"

using namespace std;

int is_input_valid(char *input, vector<string> accepted_inputs) {

  for (int i = 0; i < accepted_inputs.size(); i++) {
    if (strcmp(input, accepted_inputs.at(i).c_str()) == 0)
      return 1;
  }

  return 0;
}

void write_model_file(char *filename, vector<SimplePlan> plans) {

  ofstream file;

  file.open(filename);
}

int main(int argc, char **argv) {

  char *output_file_name = nullptr;

  vector<string> accepted_inputs = {"plane", "box",  "sphere",
                                    "cone",  "ring", "patch"};

  Model m;

  if (argc > 1 && is_input_valid(argv[1], accepted_inputs)) {

    if (strcmp(argv[1], "plane") == 0) {

      Plane p = Plane();
      m = p.generateModel();
      output_file_name = strdup(argv[2]);
    }

    if (strcmp(argv[1], "box") == 0) {

      Box b = Box(atof(argv[2]), atof(argv[3]), atof(argv[4]), atoi(argv[5]));
      m = b.generateModel();
      output_file_name = strdup(argv[6]);
    }

    if (strcmp(argv[1], "sphere") == 0) {

      Sphere s = Sphere(atof(argv[2]), atoi(argv[3]), atof(argv[4]));
      m = s.generateModel();
      output_file_name = strdup(argv[5]);
    }

    if (strcmp(argv[1], "cone") == 0) {

      Cone c = Cone(atof(argv[2]), atof(argv[3]), atoi(argv[4]), atoi(argv[5]));
      m = c.generateModel();
      output_file_name = strdup(argv[6]);
    }

    if (strcmp(argv[1], "ring") == 0) {

      Ring r = Ring(atof(argv[2]), atof(argv[3]), atoi(argv[4]));
      m = r.generateModel();
      output_file_name = strdup(argv[5]);
    }

    if (strcmp(argv[1], "patch") == 0) {

      Patch p = Patch(argv[2]);
      m = p.constructPatchedModel(atoi(argv[3]));
      output_file_name = strdup(argv[4]);
    }

    m.write_model_to_file(output_file_name);

  } else {

    cout << "Your input doesn't seem to have the proper format. \n\n";
    cout << "These are the accepted inputs for this program. \n\n";
    cout << "\t./generator plane <output_file>.3d\n";
    cout << "\t./generator box <x> <y> <z> <divisions> <output_file>.3d\n";
    cout
        << "\t./generator sphere <radius> <stacks> <slices> <output_file>.3d\n";
    cout << "\t./generator cone <bottom_radius> <height> <stacks> <slices> "
            "<output_file>.3d\n";
    cout << "\t./generator ring <inner_radius> <outter_radius> <slices> "
            "<output_file>.3d\n";
    cout << "\t./generator patch <input_file> <tesselation> <output_file>.3d\n";
  }

  return 0;
}
