#ifndef GENERATOR_H
#define GENERATOR_H
#define _USE_MATH_DEFINES

#include "models/Box.h"
#include "models/Cone.h"
#include "models/Patch.h"
#include "models/Plane.h"
#include "models/Ring.h"
#include "models/Sphere.h"
#include "utils/Point_3D.h"
#include <fstream>
#include <iostream>
#include <math.h>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <vector>

#endif // GENERATOR_GENERATOR_H