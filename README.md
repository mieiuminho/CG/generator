# Generator

This program is meant to be used as a complement to the _Engine_ program. The
`.3d` files are generated here and included in scenes that should be passed as
argument to the _Engine_.

For the time being 4 figures can be generated. To generate each of the figures
you may follow these steps:

##  Plane
`./generator plane <output_file>.3d`

## Box
`./generator box <x_dimension> <y_dimension> <z_dimension> <output_file>.3d`

## Sphere
`./generator sphere <radius> <stacks> <slices> <output_file>.3d`

## Cone
`./generator cone <bottom_radius> <height> <stacks> <slices> <output_file>.3d`

## Ring
`./generator ring <inner_radius> <outter_radius> <slices> <output_file>.3d`

## Bezier Patch
`./generator patch <input_file> <tesselation> <output_file>.3d`

## Development

Compile and run the program.

```
bin/run <args>
```

Build the target.

```
bin/build
```

Format the code.

```
bin/format
```

Clean the project directory.

```
bin/clean
```

